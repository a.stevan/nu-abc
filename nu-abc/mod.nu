use consts.nu [ CACHE, LOCK, BIB_FILE ]
use bib.nu [
    add-ref-to-bib,
    get-bib,
]

use parse.nu [ "parse bibtex" ]

# # Example
# ```nushell
# abc add --publication-type "inproceedings" --short "gennaro2010secure" {
#     title: "Secure network coding over the integers",
#     authors: [ "Gennaro, Rosario", "Katz, Jonathan", "Krawczyk, Hugo", "Rabin, Tal" ],
#     url: "https://eprint.iacr.org/2009/569.pdf",
#     booktitle: "Public Key Cryptography--PKC 2010: 13th International Conference on Practice and Theory in Public Key Cryptography, Paris, France, May 26-28, 2010. Proceedings 13",
#     pages: "142--160",
#     year: 2010,
#     organization: "Springer",
# }
# ```
export def "abc add" [
    paper: record<title: string, authors: list<string>, url: string>,
    --publication-type (-p): string = "unknown",
    --short (-s): string,
] {
    if $short == null {
        error make --unspanned { msg: $"(ansi red_bold)`--short` is required(ansi reset)" }
    }

    let ref = {
        title: $paper.title,
        authors: $paper.authors,
        url: $paper.url,
        extra: ($paper | reject title authors url),
    }

    $ref | add-ref-to-bib (metadata $paper).span

    print "following reference has been added"
    print ($ref | table --expand)
}

export def "abc clean" [] {
    rm --force --verbose $BIB_FILE
    rm --force --verbose $LOCK
}

export def "abc open" []: [ nothing -> table<title: string, authors: list<string>, url: string> ] {
    get-bib
}

def check-ref-key [ref: record, key: string, span: record<start: int, end: int>] {
    if $key not-in $ref {
        error make {
            msg: $"(ansi red)missing key(ansi reset)",
            label: {
                text: $"$.($key) is missing",
                span: $span,
            },
            help: $"list of keys: ($ref | columns)",
        }
    }
}

# # Example
# ```nushell
# abc from-bibtex "@inproceedings{gennaro2010secure,
#   title=Secure network coding over the integers},
#   author={Gennaro, Rosario and Katz, Jonathan and Krawczyk, Hugo and Rabin, Tal},
#   booktitle={Public Key Cryptography--PKC 2010: 13th International Conference on Practice and Theory in Public Key Cryptography, Paris, France, May 26-28, 2010. Proceedings 13},
#   pages={142--160},
#   year={2010},
#   organization={Springer}
# }"
# ```
export def "abc from-bibtex" [bibtex: string] {
    let bibtex_span = (metadata $bibtex).span

    let ref = $bibtex | parse bibtex $bibtex_span
    let type = $ref.type
    let short = $ref.short
    let ref = $ref
        | reject type short
        | rename --column { author: "authors" }
        | update authors { [ $in ] }

    check-ref-key $ref "title" $bibtex_span
    check-ref-key $ref "authors" $bibtex_span
    check-ref-key $ref "url" $bibtex_span

    abc add --publication-type $type --short $short $ref
}

def get-lock []: [ nothing -> record ] {
    if not ($LOCK | path exists) {
        return {}
    }

    open $LOCK | from nuon | default {}
}

export def "abc fetch" [] {
    mkdir $CACHE
    touch $LOCK

    for paper in (abc open) {
        print --no-newline $"(ansi yellow)($paper.title)(ansi reset) at (ansi cyan)($paper.url)(ansi reset)... "
        let res = try {
            let res = http get --full --allow-errors $paper.url
            print $"(ansi green_bold)done(ansi reset)"
            $res
        } catch {
            print $"(ansi red_bold)failed(ansi reset)"
            continue
        }

        let h = $res.body | hash sha256
        let pdf_file = $CACHE | path join $h
        $res.body | save --force $pdf_file

        if (do { pdfinfo $pdf_file } | complete).exit_code != 0 {
            print $"($paper.title) is not a PDF"
            rm $pdf_file
            continue
        }

        let lock = get-lock
        $lock | upsert $paper.title $h | to nuon --indent 4 | save --force $LOCK
    }
}

export def "abc read" [--reader: string] {
    if $reader == null {
        error make --unspanned { msg: $"(ansi red_bold)requires --reader(ansi reset)" }
    }
    if (which $reader | is-empty) {
        error make {
            msg: $"(ansi red)unknown reader(ansi reset)",
            label: {
                text: $"could not find in $env.PATH",
                span: (metadata $reader).span,
            },
        }
    }

    let lock = get-lock
    if $lock == {} {
        print "no PDF to read"
        return
    }

    let res = $lock | transpose k v | get k | input list --fuzzy "please choose a paper to read"
    if $res == null {
        print "user chose to exit"
        return
    }

    ^$reader ($CACHE | path join ($lock | get $res))
}
