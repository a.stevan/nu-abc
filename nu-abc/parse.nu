export def "parse bibtex" [span: record<start: int, end: int>]: [ string -> record ] {
    let raw = $in
        | lines
        | str trim
        | str join

    let tsr = $raw | parse --regex '^@(?<type>\w+){(?<short>[\w\/:.]+),(?<rest>.*)}' | into record
    if $tsr == {} {
        error make {
            msg: $"(ansi red_bold)invalid BibTeX(ansi reset)",
            label: {
                text: $"could not parse BibTeX",
                span: $span,
            },
        }
    }

    $tsr
        | update rest {
            split row '},'
                | str trim --char '}' --right
                | parse --regex '^(?<k>\w+)={(?<v>.*)'
                | transpose --header-row
                | into record
        }
        | flatten
        | into record
}
