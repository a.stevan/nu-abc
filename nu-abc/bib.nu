use consts.nu BIB_FILE

export def get-bib []: [ nothing -> table<title: string, authors: list<string>, url: string> ] {
    if not ($BIB_FILE | path exists) {
        return []
    }

    open $BIB_FILE
}

export def add-ref-to-bib [span: record<start: int, end: int>] {
    let ref = $in
    let bib = get-bib

    if $ref.title in $bib.title {
        error make {
            msg: $"(ansi red_bold)duplicate reference(ansi reset)",
            label: {
                text: $"already in ($BIB_FILE)",
                span: $span,
            },
            help: $"'(ansi yellow)($ref.title)(ansi reset)' has been found in ($BIB_FILE)"
        }
    }
    $bib | append $ref | to nuon -i 4 | save --force $BIB_FILE
}
