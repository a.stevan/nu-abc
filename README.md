# Academic Bibliography Cruiser
A Nushell module that help cruising academic papers to build a bibliography.

## installation
- install [Nupm](https://github.com/nushell/nupm)
- bring the module into current scope with `use nupm`
- install `nu-abc`
```bash
nupm install --path . --force
```

## usage
- bring `nu-abc` into scope
```bash
use nu-abc
```
- get some help
```bash
nu-abc --help
```
